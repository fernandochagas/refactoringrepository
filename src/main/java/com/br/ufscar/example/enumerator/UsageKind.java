/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.enumerator;

/**
 *
 * @author Fernando Chagas
 */
public enum UsageKind {
    
    RENAME, 
    DEALING_WITH_GENERALIZATION, 
    MOVING_FEATURES_BETWEEN_OBJETS, //milho do rafa
    ORGANIZATION_DATA,
    UNKNOWN
    
}
