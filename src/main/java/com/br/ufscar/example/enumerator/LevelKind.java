/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.enumerator;

/**
 *
 * @author Fernando Chagas
 */
public enum LevelKind {
    
    CODE_LEVEL, ARCHITECTURE_LEVEL
    
}
