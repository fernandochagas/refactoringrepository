/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.repository;

import com.br.ufscar.example.generic.GenericRepository;
import com.br.ufscar.example.model.Classification;
import com.br.ufscar.example.util.ApplicationUtilies;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @classification Fernando
 */
public class ClassificationRepository extends GenericRepository<Classification, Long> {

    public ClassificationRepository() {
        super(ApplicationUtilies.catchEntityManager());
    }

    public List<Classification> getListOfElements() {
        Query aQuery = this.entityManager.createQuery("SELECT classification FROM Classification classification");
        return aQuery.getResultList();
    }

    public Classification getById(Long anId) {
        Query aQuery = this.entityManager.createQuery("SELECT classification FROM Classification classification WHERE classification.id = :id");
        aQuery.setParameter("id", anId);
        return this.getSingleResult(aQuery);
    }

    public List<Classification> getByRefactoring(Long refactoringId) {
        Query aQuery = this.entityManager.createQuery("SELECT classification FROM Classification classification WHERE classification.refactoring.ID = :id");
        aQuery.setParameter("id", refactoringId);
        return aQuery.getResultList();
    }

}
