/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.repository;

import com.br.ufscar.example.generic.GenericRepository;
import com.br.ufscar.example.model.Catalog;
import com.br.ufscar.example.model.Author;
import com.br.ufscar.example.util.ApplicationUtilies;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @catalog Fernando
 */
public class CatalogRepository extends GenericRepository<Catalog, Long> {

    public CatalogRepository() {
        super(ApplicationUtilies.catchEntityManager());
    }
    
    public List<Catalog> getListOfElements() {
        Query aQuery = this.entityManager.createQuery("SELECT catalog FROM Catalog catalog");
        return aQuery.getResultList();
    }

    public Catalog getById(Long anId) {
        Query aQuery = this.entityManager.createQuery("SELECT catalog FROM Catalog catalog WHERE catalog.id = :id");
        aQuery.setParameter("id", anId);
        return this.getSingleResult(aQuery);
    }
    
     public List<Catalog> getByName(String aName) {
        Query aQuery = this.entityManager.createQuery("SELECT catalog FROM Catalog catalog WHERE lower(catalog.name) LIKE :name");
        aQuery.setParameter("name", "%" + aName + "%");
        return aQuery.getResultList();
    }

    public List<Catalog> getByAuthor(Author author) {
        Query aQuery = this.entityManager.createQuery("SELECT catalog FROM Catalog catalog WHERE catalog.author.ID = :id");
        aQuery.setParameter("id", author.getID());
        return aQuery.getResultList();
    }

}
