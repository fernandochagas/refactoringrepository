/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.repository;

import com.br.ufscar.example.generic.GenericRepository;
import com.br.ufscar.example.model.RefactoringLibrary;
import com.br.ufscar.example.model.RefactoringModel;
import com.br.ufscar.example.util.ApplicationUtilies;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Fernando
 */
public class RefactoringModelRepository extends GenericRepository<RefactoringModel, Long> {

    public RefactoringModelRepository() {
        super(ApplicationUtilies.catchEntityManager());
    }
    
    public List<RefactoringLibrary> getListOfElements() {
        Query aQuery = this.entityManager.createQuery("SELECT refactoringModel FROM RefactoringModel refactoringModel");
        return aQuery.getResultList();
    }

}
