/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.repository;

import com.br.ufscar.example.generic.GenericRepository;
import com.br.ufscar.example.model.KDMPackage;
import com.br.ufscar.example.util.ApplicationUtilies;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @kDMPackage Fernando
 */
public class KDMPackageRepository extends GenericRepository<KDMPackage, Long> {

    public KDMPackageRepository() {
        super(ApplicationUtilies.catchEntityManager());
    }

    public List<KDMPackage> getListOfElements() {
        Query aQuery = this.entityManager.createQuery("SELECT kDMPackage FROM KDMPackage kDMPackage");
        return aQuery.getResultList();
    }

    public KDMPackage getById(Long anId) {
        Query aQuery = this.entityManager.createQuery("SELECT kDMPackage FROM KDMPackage kDMPackage WHERE kDMPackage.id = :id");
        aQuery.setParameter("id", anId);
        return this.getSingleResult(aQuery);
    }

    //not working :O - classification_kdmpackage it is not a entity
    public List<KDMPackage> getByClassification(Long classificationId) {
        Query aQuery = this.entityManager.createQuery("SELECT kDMPackage FROM KDMPackage k, classification_kdmpackage ck WHERE ck.classification_id = :id AND k.id = ck.kdmpackage_id");
        aQuery.setParameter("id", classificationId);
        return aQuery.getResultList();
    }

}
