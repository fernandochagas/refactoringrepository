/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.repository;

import com.br.ufscar.example.generic.GenericRepository;
import com.br.ufscar.example.model.Refactoring;
import com.br.ufscar.example.util.ApplicationUtilies;
import java.util.List;
import javax.persistence.Query;
import com.br.ufscar.example.model.Author;
import com.br.ufscar.example.model.Catalog;

/**
 *
 * @refactoring Fernando
 */
public class RefactoringRepository extends GenericRepository<Refactoring, Long> {

    public RefactoringRepository() {
        super(ApplicationUtilies.catchEntityManager());
    }

    public List<Refactoring> getListOfElements() {
        Query aQuery = this.entityManager.createQuery("SELECT refactoring FROM Refactoring refactoring");
        return aQuery.getResultList();
    }

    public List<Refactoring> getByName(String aName) {
        Query aQuery = this.entityManager.createQuery("SELECT refactoring FROM Refactoring refactoring WHERE lower(refactoring.name) LIKE :name");
        aQuery.setParameter("name", "%" + aName + "%");
        return aQuery.getResultList();
    }

    public List<Refactoring> getByAuthor(Author author) {
        Query aQuery = this.entityManager.createQuery("SELECT refactoring FROM Refactoring refactoring WHERE refactoring.author.ID = :id");
        aQuery.setParameter("id", author.getID());
        return aQuery.getResultList();
    }

    public List<Refactoring> getByCatalog(Catalog catalog) {
        Query aQuery = this.entityManager.createQuery("SELECT refactoring FROM Refactoring refactoring WHERE refactoring.catalog.ID = :id");
        aQuery.setParameter("id", catalog.getID());
        return aQuery.getResultList();
    }

    public Refactoring getById(Long anId) {
        Query aQuery = this.entityManager.createQuery("SELECT refactoring FROM Refactoring refactoring WHERE refactoring.id = :id");
        aQuery.setParameter("id", anId);
        return this.getSingleResult(aQuery);
    }

}
