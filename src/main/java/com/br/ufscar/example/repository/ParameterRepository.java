/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.repository;

import com.br.ufscar.example.generic.GenericRepository;
import com.br.ufscar.example.model.Parameter;
import com.br.ufscar.example.util.ApplicationUtilies;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @parameter Fernando
 */
public class ParameterRepository extends GenericRepository<Parameter, Long> {

    public ParameterRepository() {
        super(ApplicationUtilies.catchEntityManager());
    }

    public List<Parameter> getListOfElements() {
        Query aQuery = this.entityManager.createQuery("SELECT parameter FROM Parameter parameter");
        return aQuery.getResultList();
    }

    public Parameter getById(Long anId) {
        Query aQuery = this.entityManager.createQuery("SELECT parameter FROM Parameter parameter WHERE parameter.id = :id");
        aQuery.setParameter("id", anId);
        return this.getSingleResult(aQuery);
    }
    
    public List<Parameter> getByRefactoring(Long refactoringId) {
        Query aQuery = this.entityManager.createQuery("SELECT parameter FROM Parameter parameter WHERE parameter.refactoring.ID = :id");
        aQuery.setParameter("id", refactoringId);
        return aQuery.getResultList();
    }

}
