/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.repository;

import com.br.ufscar.example.generic.GenericRepository;
import com.br.ufscar.example.model.Author;
import com.br.ufscar.example.util.ApplicationUtilies;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Fernando
 */
public class AuthorRepository extends GenericRepository<Author, Long> {

    public AuthorRepository() {
        super(ApplicationUtilies.catchEntityManager());
    }

    public List<Author> getListOfElements() {
        Query aQuery = this.entityManager.createQuery("SELECT author FROM Author author");
        return aQuery.getResultList();
    }

    public Author getById(Long anId) {
        Query aQuery = this.entityManager.createQuery("SELECT author FROM Author author WHERE author.id = :id");
        aQuery.setParameter("id", anId);
        return this.getSingleResult(aQuery);
    }

}
