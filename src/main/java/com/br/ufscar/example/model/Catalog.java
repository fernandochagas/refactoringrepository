/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Fernando
 */
@Entity
public class Catalog implements Serializable {
    @Id
    @GeneratedValue
    private Long ID;
    private String name;
    @ManyToOne
    private RefactoringLibrary refactoringLibrary;
    @ManyToOne
    private Author author;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public RefactoringLibrary getRefactoringLibrary() {
        return refactoringLibrary;
    }

    public void setRefactoringLibrary(RefactoringLibrary refactoringLibrary) {
        this.refactoringLibrary = refactoringLibrary;
    }
    
}