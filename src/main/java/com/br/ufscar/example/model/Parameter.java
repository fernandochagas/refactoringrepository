/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Fernando
 */
@Entity
public class Parameter implements Serializable{
    @Id
    @GeneratedValue
    private Long ID;
    private String parameter;
    @ManyToOne
    private Refactoring refactoring;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public Refactoring getRefactoring() {
        return refactoring;
    }

    public void setRefactoring(Refactoring refactoring) {
        this.refactoring = refactoring;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }
    
}
