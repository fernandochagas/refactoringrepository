/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author Fernando
 */
@Entity
public class Refactoring implements Serializable{
    @Id
    @GeneratedValue
    private Long ID;
    private String name;
    @Length(max = 10000)
    private String motivation;
    @Length(max = 10000)
    private String summary;
    @ManyToOne
    private Author author;
    @ManyToOne
    private Catalog catalog;
   
    @ManyToOne(cascade = CascadeType.ALL)
    private Operator operator = new Operator();
    @ManyToOne(cascade = CascadeType.ALL)
    private PreCondition preCondition = new PreCondition();
    @ManyToOne(cascade = CascadeType.ALL)
    private PostCondition postCondition = new PostCondition();
    
    @OneToMany(mappedBy = "")
    private List<Refactoring> chainOfRefactoring = new ArrayList<>();

    public Refactoring() {
        this.operator = new Operator();
        this.preCondition = new PreCondition();
        this.postCondition = new PostCondition();
    }    

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public PreCondition getPreCondition() {
        return preCondition;
    }

    public void setPreCondition(PreCondition preCondition) {
        this.preCondition = preCondition;
    }

    public PostCondition getPostCondition() {
        return postCondition;
    }

    public void setPostCondition(PostCondition postCondition) {
        this.postCondition = postCondition;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public List<Refactoring> getChainOfRefactoring() {
        System.err.println (chainOfRefactoring.size());
        return chainOfRefactoring;
    }

    public void setChainOfRefactoring(List<Refactoring> chainOfRefactoring) {
        this.chainOfRefactoring = chainOfRefactoring;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
    
}
