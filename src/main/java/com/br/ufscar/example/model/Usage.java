/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.model;

import com.br.ufscar.example.enumerator.UsageKind;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Fernando Chagas
*/
@Entity
public class Usage implements Serializable{
    
    @Id
    @GeneratedValue
    private Long ID;
    
    private UsageKind kind;

    public UsageKind getKind() {
        return kind;
    }

    public void setKind(UsageKind kind) {
        this.kind = kind;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }
    
}
