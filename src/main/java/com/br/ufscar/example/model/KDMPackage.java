/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.model;

import com.br.ufscar.example.enumerator.KDMPackageKind;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Fernando Chagas
 */
@Entity
public class KDMPackage implements Serializable {
  
    @Id
    @GeneratedValue
    private Long ID;
    private KDMPackageKind kind;
    
    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public KDMPackageKind getKind() {
        return kind;
    }

    public void setKind(KDMPackageKind kind) {
        this.kind = kind;
    }
    
}
