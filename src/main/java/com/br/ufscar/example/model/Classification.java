/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Fernando Chagas
 */
@Entity
public class Classification implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;
        
    @ManyToOne(cascade = CascadeType.ALL)
    private Level level = new Level();
    @ManyToOne(cascade = CascadeType.ALL)
    private Usage usage = new Usage();
    
    
    @OneToMany(cascade = CascadeType.ALL)
    private List<KDMPackage> kdmPackage = new ArrayList<>();
    
    @ManyToOne
    private Refactoring refactoring;
    

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    public Refactoring getRefactoring() {
        return refactoring;
    }

    public void setRefactoring(Refactoring refactoring) {
        this.refactoring = refactoring;
    }

    public List<KDMPackage> getKdmPackage() {
        return kdmPackage;
    }

    public void setKdmPackage(List<KDMPackage> kdmPackage) {
        this.kdmPackage = kdmPackage;
    }

    
    
}
