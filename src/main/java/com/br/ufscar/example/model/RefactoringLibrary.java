/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.validator.constraints.Length;

/**
 *
 * @author Fernando
 */
@Entity
public class RefactoringLibrary implements Serializable {
    @Id
    @GeneratedValue
    private Long ID;
    private String name;
    @Length(max = 500)
    private String shortDescription;
    @Length(max = 10000)
    private String description;
    @ManyToOne
    private Author author;
    @ManyToOne 
    private RefactoringModel refactoringModel;

    
    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public RefactoringModel getRefactoringModel() {
        return refactoringModel;
    }

    public void setRefactoringModel(RefactoringModel refactoringModel) {
        this.refactoringModel = refactoringModel;
    }
    
}
