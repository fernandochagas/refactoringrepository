/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.model;

import com.br.ufscar.example.enumerator.LevelKind;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Fernando Chagas
 */
@Entity
public class Level implements Serializable{
   
    @Id
    @GeneratedValue
    private Long ID;
    
    private LevelKind kind;

    public LevelKind getKind() {
        return kind;
    }

    public void setKind(LevelKind kind) {
        this.kind = kind;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }
    
}
