package com.br.ufscar.example.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.util.Iterator;
import java.util.Random;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import org.primefaces.context.RequestContext;

/**
 * Classe com diversas funções úteis em todo o contexto da aplicação
 *
 * @author Marcelo Claudio
 * @version 2.0
 */
public abstract class ApplicationUtilies {

    private static Random random = new Random(System.currentTimeMillis());
    
    /**
     * Obtém um Entity Manager presente na sessão
     *
     * @since 1.0
     * @return Entity Manager presente na sessão
     */
    public static EntityManager catchEntityManager() {
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        EntityManager entityManager = (EntityManager) FacesContext.getCurrentInstance().
                getApplication().getELResolver().getValue(elContext, null, "entityManager");
        return entityManager;
    }

    /**
     * Obtém o usuário logado do contexto do Spring Security 3
     *
     * @since 2.0
     * @return Usuário logado
     */

    public static String generateMD5(String aToken) {
        MessageDigest aMessageDigest = null;
        try {
            aMessageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Algoritmo solicitado não encontrado.");
        }
        BigInteger hash = new BigInteger(1, aMessageDigest.digest(aToken.getBytes()));
        String aMD5 = hash.toString(16);
        return aMD5;
    }

    public static void facesMessage(String aTitle, String aMessage, FacesMessage.Severity aSeverity) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        FacesMessage facesMessage = new FacesMessage(aSeverity, aTitle, aMessage);
        facesContext.addMessage(null, facesMessage);
    }

    public static void executeJQuery(String aCommand) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute(aCommand);
    }

    public static void cleanFacesMessage() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Iterator<FacesMessage> msgIterator = FacesContext.getCurrentInstance().getMessages();
        while (msgIterator.hasNext()) {
            msgIterator.next();
            msgIterator.remove();
        }
        System.err.println(facesContext.getMessageList().size());
        System.err.println(facesContext.getMessageList());

    }

    public static String generateURL(String toURL) {
        CharSequence aCharSequence = new StringBuilder(toURL);
        String url = Normalizer.normalize(aCharSequence, Normalizer.Form.NFKD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        url = url.replace(" ", "-");
        url = url.replace(".", "");
        url = url.replace("/", "");
        url = url.replace("+", "");
        url = url.replace(":", "");
        url = url.replace("=", "");
        url = url.replace("'", "");
        url = url.replace(",", "");
        url = url.replace("(", "");
        url = url.replace(")", "");
        url = url.toLowerCase();
        return url;
    }
}