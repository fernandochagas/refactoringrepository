package com.br.ufscar.example.generic;

import javax.enterprise.context.Conversation;
import javax.inject.Inject;

/**
 * Classe modelo para ManagedBeans
 *
 * @author Marcelo Claudio
 * @version 1.0
 */
public abstract class GenericConversationController<Class, RepositoryClass> extends GenericController<Class, RepositoryClass> {

    @Inject
    protected Conversation conversation;

    public void beginConversation() {
        if (this.conversation.isTransient()) {
            this.conversation.begin();
            System.err.println("Conversation Started" + this.conversation.getId());
        }
    }

    public void endConversation() {
        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
    }
}
