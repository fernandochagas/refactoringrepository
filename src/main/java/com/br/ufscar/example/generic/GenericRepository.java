package com.br.ufscar.example.generic;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Classe modelo para repositórios definidos na aplicação
 *
 * @author Marcelo Claudio
 * @version 2.0
 * @param <Class>
 * @param <PKType>
 */
public abstract class GenericRepository<Class, PKType> {

    protected EntityManager entityManager;

    /**
     * Construtor da classe
     *
     * @param anEntityManager Entity Manager para realização das transações no
     * banco de dados
     * @since 1.0
     */
    public GenericRepository(EntityManager anEntityManager) {
        this.entityManager = anEntityManager;
    }

    /**
     * Método resposável por inserção no banco de dados.
     *
     * @return 
     * @since 1.0
     * @param anInsert Objeto a ser inserido
     */
    public boolean insert(Class anInsert) {
        try {
            this.entityManager.persist(anInsert);
            this.entityManager.flush();
        } catch (Exception exception) {
            System.err.println(exception);
            //FacesMessage t = new FacesMessage(FacesMessage.SEVERITY_ERROR, exception.getConstraintViolations().iterator().next().getMessage(), null);
            //FacesContext.getCurrentInstance().addMessage(null, t);
            return false;
        }
        return true;
    }

    /**
     * Método resposável por remoção no banco de dados.
     *
     * @param aRemoval
     * @since 1.0
     */
    public void remove(Class aRemoval) {
        this.entityManager.remove(this.entityManager.merge(aRemoval));
        this.entityManager.flush();
    }

    /**
     * Método resposável por atuazalização no banco de dados.
     *
     * @return 
     * @since 1.0
     * @param anUpdate Objeto a ser atualizado
     */
    public boolean update(Class anUpdate) {
         try {
        this.entityManager.merge(anUpdate);
            this.entityManager.flush();
        } catch (Exception exception) {
            System.err.println(exception);
            //FacesMessage t = new FacesMessage(FacesMessage.SEVERITY_ERROR, exception.getConstraintViolations().iterator().next().getMessage(), null);
            //FacesContext.getCurrentInstance().addMessage(null, t);
            return false;
        }
        return true;
    }

    /**
     * Método responsável por retornar um único resultado de uma busca.
     *
     * @since 2.0
     * @param aQuery
     * @return Primeiro resultado da busca.
     */
    public Class getSingleResult(Query aQuery) {
        aQuery.setMaxResults(1); //limita a busca a um elemento
        List<Class> aListResult = aQuery.getResultList(); //recebe a lista com os resultados
        if (aListResult == null || aListResult.isEmpty()) //verifica se é nulo ou vazia
        {
            return null;
        } else {
            return (Class) aListResult.get(0); //caso não seja envia o primeiro resultado        
        }
    }

   /* public abstract Class getById(PKType anId);

    public abstract List<Class> getListOfElements();*/
}
