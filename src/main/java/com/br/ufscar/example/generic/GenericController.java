package com.br.ufscar.example.generic;

import com.br.ufscar.example.util.ApplicationUtilies;
import java.io.Serializable;
import java.util.List;

/**
 * Classe modelo para ManagedBeans
 * @author Marcelo Claudio
 * @version 1.0
 * @param <Class>
 * @param <RepositoryClass>
 */
public abstract class GenericController <Class,RepositoryClass> implements 
        Serializable{
    
    protected List<Class> listOfEntities;
    protected RepositoryClass repository;
    protected Class entity;

    /**
     * Retorna um Entity Manager
     * @since 1.0
     * @return Uma instância da classe que o bean gerencia.
     */
    public Class getEntity() {
        return entity;
    }

    /**
     * Define um Entity Manager
     * @since 1.0
     * @param anEntity Uma instância da classe que o bean gerencia.
     */
    public void setEntity(Class anEntity) {
        this.entity = anEntity;
    }

    /**
     * Retorna uma collection da classe que o bean gerencia.
     * @since 1.0
     * @return Collection de instâncias da classe gerenciada
     */
    public List<Class> getListOfEntities() {
        return listOfEntities;
    }

    /**
     * Define uma collection da classe que o bean gerencia.
     * @since 1.0
     * @param aListOfEntities Collection de instâncias da classe gerenciada
     */
    public void setListOfEntities(List<Class> aListOfEntities) {
        this.listOfEntities = aListOfEntities;
    }
    
     
    /**
     * Retorna uma instância do  Repository responsável pela classe gerenciada 
     * pelo Managed Bean.
     * @since 1.0
     * @return Uma instância do Repository
     */
    public RepositoryClass getRepository() {
        return repository;
    }
    
    /**
     * Método abstrato resposável pela requisição de inserção no Banco de Dados.
     */
    abstract public void insert();
    
    /**
     * Método abstrato resposável pela requisição de remoção no Banco de Dados.
     */
    abstract public void remove();
    
    /**
     * Método abstrato reposável pela requisição de atualização no Banco de 
     * Dados.
     */
    abstract public void update();
    
    public void cleanFacesMessage(){
        ApplicationUtilies.cleanFacesMessage();
    }
}
