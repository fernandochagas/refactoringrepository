/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.controller;

import com.br.ufscar.example.generic.GenericController;
import com.br.ufscar.example.model.Classification;
import com.br.ufscar.example.model.Refactoring;
import com.br.ufscar.example.repository.ClassificationRepository;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @classification Fernando
 */
@Named
@SessionScoped
public class ClassificationController extends GenericController<Classification, ClassificationRepository> {

    public ClassificationController() {
        this.entity = new Classification();
    }

    @Override
    public void insert() {
        this.repository = new ClassificationRepository();
        this.repository.insert(this.entity);
        this.entity = new Classification();

        //message
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "New Classification added"));
    }

    @Override
    public void remove() {
        this.repository = new ClassificationRepository();
        this.repository.remove(this.entity);
        this.entity = new Classification();
    }

    @Override
    public void update() {
        this.repository = new ClassificationRepository();
        this.repository.update(this.entity);
        this.entity = new Classification();
    }

    @Override
    public List<Classification> getListOfEntities() {
        this.repository = new ClassificationRepository();
        this.listOfEntities = this.repository.getListOfElements();
        return this.listOfEntities;
    }
    
    public List<Classification> getByRefactoring (Long refactoringId) {
        this.repository = new ClassificationRepository();
        return this.repository.getByRefactoring(refactoringId);
    }
    
}
