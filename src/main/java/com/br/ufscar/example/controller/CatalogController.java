/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.controller;

import com.br.ufscar.example.generic.GenericController;
import com.br.ufscar.example.model.Catalog;
import com.br.ufscar.example.model.Author;
import com.br.ufscar.example.repository.CatalogRepository;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Fernando
 */
@Named
@SessionScoped
public class CatalogController extends GenericController<Catalog, CatalogRepository> {
    
    private String catalogName;
    private List<Catalog> catalogsFound = new ArrayList<>();
    private Author author;

    public CatalogController() {
        this.entity = new Catalog();
    }

    @Override
    public void insert() {
        this.repository = new CatalogRepository();
        this.repository.insert(this.entity);
        this.entity = new Catalog();

        //message
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "New Catalog added"));
    }

    @Override
    public void remove() {
        this.repository = new CatalogRepository();
        this.repository.remove(this.entity);
        this.entity = new Catalog();
    }

    @Override
    public void update() {
        this.repository = new CatalogRepository();
        this.repository.update(this.entity);
        this.entity = new Catalog();
    }
    
     public List<Catalog> completeCatalog (String query) {
        this.repository = new CatalogRepository();
        List<Catalog> filteredCatalogs = new ArrayList<>();
        this.listOfEntities = this.repository.getListOfElements();
        for (int i = 0; i < this.listOfEntities.size(); i++) {
            String refactoringName = this.listOfEntities.get(i).getName();
            if (refactoringName.toLowerCase().contains(query.toLowerCase())) {
                filteredCatalogs.add(this.listOfEntities.get(i));
            }
        }
        return filteredCatalogs;
    }
     
      public void getByName () {
        this.repository = new CatalogRepository();
        this.catalogsFound = this.repository.getByName(catalogName.toLowerCase());
        this.catalogName = "";
    }
    
    public void getByAuthor () {
        this.repository = new CatalogRepository();
        this.catalogsFound = this.repository.getByAuthor(this.author);
        this.author = new Author();
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public List<Catalog> getCatalogsFound() {
        return catalogsFound;
    }

    public void setCatalogsFound(List<Catalog> catalogsFound) {
        this.catalogsFound = catalogsFound;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
    
    

}
