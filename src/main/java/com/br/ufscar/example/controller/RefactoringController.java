/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.controller;

import com.br.ufscar.example.enumerator.KDMPackageKind;
import com.br.ufscar.example.generic.GenericController;
import com.br.ufscar.example.model.Author;
import com.br.ufscar.example.model.Catalog;
import com.br.ufscar.example.enumerator.Language;
import com.br.ufscar.example.enumerator.LevelKind;
import com.br.ufscar.example.enumerator.UsageKind;
import com.br.ufscar.example.model.Classification;
import com.br.ufscar.example.model.KDMPackage;
import com.br.ufscar.example.model.Level;
import com.br.ufscar.example.model.Operator;
import com.br.ufscar.example.model.Parameter;
import com.br.ufscar.example.model.PostCondition;
import com.br.ufscar.example.model.PreCondition;
import com.br.ufscar.example.model.Refactoring;
import com.br.ufscar.example.model.RefactoringLibrary;
import com.br.ufscar.example.model.RefactoringModel;
import com.br.ufscar.example.model.Usage;
import com.br.ufscar.example.repository.ClassificationRepository;
import com.br.ufscar.example.repository.KDMPackageRepository;
import com.br.ufscar.example.repository.ParameterRepository;
import com.br.ufscar.example.repository.RefactoringModelRepository;
import com.br.ufscar.example.repository.RefactoringRepository;
import com.br.ufscar.example.util.ApplicationUtilies;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import refactoringModel.refactoringModel.RefactoringModelFactory;
import refactoringModel.refactoringModel.RefactoringModelPackage;

/**
 *
 * @refactoring Fernando
 */
@URLMappings(mappings = {
    @URLMapping(id = "refByCatalog", pattern = "/search/refByCatalog", viewId = "/view/search/refByCatalog.xhtml"),
    @URLMapping(id = "refByName", pattern = "/search/refByName", viewId = "/view/search/refByName.xhtml"),
    @URLMapping(id = "newRefactoring", pattern = "/admin/refactoring/new", viewId = "/view/admin/newRefactoring.xhtml")
})

@Named
@SessionScoped
public class RefactoringController extends GenericController<Refactoring, RefactoringRepository> {

    @Inject
    private FileController fileController;
    @Inject
    private ClassificationController classificationController;
    @Inject
    private AuthorController authorController;
    @Inject
    private ParameterController parameterController;
    @Inject
    private RefactoringLibraryController refactoringLibraryController;
    @Inject
    private CatalogController catalogController;

    private List<Language> allLanguages;
    private List<Parameter> parametersToAdd = new ArrayList<>();
    private Parameter parameter = new Parameter();
    private String refactoringName;
    private List<Refactoring> entitiesFound = new ArrayList<>();
    private Author author = new Author();
    private Catalog catalog = new Catalog();

    private List<Classification> classificationsToAdd = new ArrayList<>();
    private Classification classification = new Classification();

    private List<UsageKind> allUsages;
    private List<KDMPackageKind> allKDMPackageKinds;
    private KDMPackage kDMPackage;
    private KDMPackageKind kDMPackageKind;
    private List<LevelKind> allLevelKinds;

    private List<Refactoring> allChain = new ArrayList<>();

    private Refactoring auxRefactoring = new Refactoring();

    public RefactoringController() {
        this.entity = new Refactoring();
    }

    @URLAction(mappingId = "refByCatalog")
    public void urlRefByCatalog() {
        if (this.catalog.getID() != null) {
            this.getByCatalog();
        }
    }

    @URLAction(mappingId = "newRefactoring")
    public void urlNewRefactoring() {
        this.entity = new Refactoring();
    }

    @Override
    public void insert() {
        this.repository = new RefactoringRepository();

        if (this.entity.getOperator().getBody().equals("")) {
            FacesContext context = FacesContext.getCurrentInstance();

            context.addMessage(null, new FacesMessage("Error", "Put a body on your operation!"));
            return;
        }

        if (this.entity.getPreCondition().getContext().equals("")) {
            this.entity.setPreCondition(null);
        }
        if (this.entity.getPostCondition().getContext().equals("")) {
            this.entity.setPostCondition(null);
        }

        if (this.allChain.size() > 0) {
            this.entity.setChainOfRefactoring(this.allChain);
        }

        this.repository.insert(this.entity);

        ParameterRepository parameterRepository = new ParameterRepository();
        for (Parameter parameterToAdd : parametersToAdd) {
            parameterToAdd.setRefactoring(this.entity);
            parameterRepository.insert(parameterToAdd);
        }

        ClassificationRepository classificationRepository = new ClassificationRepository();
        for (Classification classificationToAdd : classificationsToAdd) {
            classificationToAdd.setRefactoring(this.entity);
            classificationRepository.insert(classificationToAdd);
        }

        this.entity = new Refactoring();
        this.parametersToAdd = new ArrayList<>();
        this.parameter = new Parameter();
        this.classificationsToAdd = new ArrayList<>();
        this.classification = new Classification();
        this.allChain = new ArrayList<>();

        //message
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "New Refactoring added"));
    }

    @Override
    public void remove() {
        this.repository = new RefactoringRepository();
        this.repository.remove(this.entity);
        this.entity = new Refactoring();
    }

    @Override
    public void update() {
        this.repository = new RefactoringRepository();
        this.repository.update(this.entity);
        this.entity = new Refactoring();
    }

    @Override
    public List<Refactoring> getListOfEntities() {
        this.repository = new RefactoringRepository();
        this.listOfEntities = this.repository.getListOfElements();
        return this.listOfEntities;
    }

    public void addParameter(ActionEvent event) {
        if (parameter.getParameter().equals("")) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Erro", "Add a message to parameter"));
        } else {
            this.parametersToAdd.add(parameter);
            this.parameter = new Parameter();
        }
    }

    public void addClassification(ActionEvent event) {
        if (classification.getUsage().equals("") || classification.getLevel().equals("") || classification.getKdmPackage().equals("")) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Erro", "Classification validation error."));
        } else {
            this.classificationsToAdd.add(classification);
            this.classification = new Classification();
        }
    }

    public void getByName() {
        this.repository = new RefactoringRepository();
        this.entitiesFound = this.repository.getByName(refactoringName.toLowerCase());
        this.refactoringName = "";
    }

    public void getByAuthor() {
        this.repository = new RefactoringRepository();
        this.entitiesFound = this.repository.getByAuthor(this.author);
        this.author = new Author();
    }

    public void getByCatalog() {
        this.repository = new RefactoringRepository();
        this.entitiesFound = this.repository.getByCatalog(this.catalog);
        this.catalog = new Catalog();
    }

    public List<Refactoring> completeRefactoring(String query) {
        this.repository = new RefactoringRepository();
        return this.repository.getByName(query.toLowerCase());
    }

    public List<UsageKind> getAllUsages() {
        return Arrays.asList(UsageKind.values());
    }

    public void setAllUsages(List<UsageKind> allUsages) {
        this.allUsages = allUsages;
    }

    public List<KDMPackageKind> getAllKDMPackageKinds() {
        return Arrays.asList(KDMPackageKind.values());
    }

    public void setAllKDMPackageKinds(List<KDMPackageKind> allKDMPackageKinds) {
        this.allKDMPackageKinds = allKDMPackageKinds;
    }

    public List<LevelKind> getAllLevelKinds() {
        return Arrays.asList(LevelKind.values());
    }

    public void setAllLevelKinds(List<LevelKind> allLevelKinds) {
        this.allLevelKinds = allLevelKinds;
    }

    public List<Language> getAllLanguages() {
        return Arrays.asList(Language.values());
    }

    public void setAllLanguages(List<Language> allLanguages) {
        this.allLanguages = allLanguages;
    }

    public List<Parameter> getParametersToAdd() {
        return parametersToAdd;
    }

    public void setParametersToAdd(List<Parameter> parametersToAdd) {
        this.parametersToAdd = parametersToAdd;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    public String getRefactoringName() {
        return refactoringName;
    }

    public void setRefactoringName(String refactoringName) {
        this.refactoringName = refactoringName;
    }

    public List<Refactoring> getEntitiesFound() {
        return entitiesFound;
    }

    public void setEntitiesFound(List<Refactoring> entitiesFound) {
        this.entitiesFound = entitiesFound;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        System.err.println(catalog.getName());

        this.catalog = catalog;
    }

    public List<Classification> getClassificationsToAdd() {
        return classificationsToAdd;
    }

    public void setClassificationsToAdd(List<Classification> classificationsToAdd) {
        this.classificationsToAdd = classificationsToAdd;
    }

    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    public Refactoring getAuxRefactoring() {
        return auxRefactoring;
    }

    public void setAuxRefactoring(Refactoring auxRefactoring) {
        this.auxRefactoring = auxRefactoring;
    }

    public void addChain() {
        this.allChain.add(this.auxRefactoring);
        this.auxRefactoring = new Refactoring();
    }

    public List<Refactoring> getAllChain() {
        return allChain;
    }

    public void setAllChain(List<Refactoring> allChain) {
        this.allChain = allChain;
    }

    public KDMPackage getkDMPackage() {
        return kDMPackage;
    }

    public void setkDMPackage(KDMPackage kDMPackage) {
        this.kDMPackage = kDMPackage;
    }

    public KDMPackageKind getkDMPackageKind() {
        return kDMPackageKind;
    }

    public void setkDMPackageKind(KDMPackageKind kDMPackageKind) {
        this.kDMPackageKind = kDMPackageKind;
    }
      
    
    public void teste() {

        ResourceSet resourceSet = new ResourceSetImpl();

        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
                new XMIResourceFactoryImpl());

        resourceSet.getPackageRegistry().put(RefactoringModelPackage.eNS_URI,
                RefactoringModelPackage.eINSTANCE);

        URI uri = fileController.getFile().getFileName() != null ? URI.createFileURI("/uploads/" + fileController.getFile().getFileName()) : URI.createURI("C:\\teste2.refactoringmodel");

        try {

            Resource resource = resourceSet.getResource(uri, true);

            System.out.println("Loaded " + uri);

            for (EObject eObject : resource.getContents()) {

                if (eObject instanceof refactoringModel.refactoringModel.RefactoringModel) {

                    refactoringModel.refactoringModel.RefactoringModel myRefactorinModel = (refactoringModel.refactoringModel.RefactoringModel) eObject;
                    System.out.println("AUthor " + myRefactorinModel.getAuthor().getName());

                    RefactoringModel refactoringModel = new RefactoringModel();
                    RefactoringModelRepository refactoringModelRepository = new RefactoringModelRepository();

                    Author authorRM = new Author();
                    authorRM.setName(myRefactorinModel.getAuthor().getName());
                    authorRM.setLastName(myRefactorinModel.getAuthor().getLastName());
                    refactoringModel.setAuthor(authorRM);

                    authorController.setEntity(authorRM);
                    authorController.insert();

                    refactoringModelRepository.insert(refactoringModel);

                    EList<refactoringModel.refactoringModel.RefactoringLibrary> libraries = myRefactorinModel.getLibraries();

                    RefactoringLibrary refactoringLibrary = null;
                    Author authorRL = null;

                    //Carregando Bibliotecas
                    for (refactoringModel.refactoringModel.RefactoringLibrary library : libraries) {

                        refactoringLibrary = new RefactoringLibrary();

                        refactoringLibrary.setDescription(library.getDescription());
                        refactoringLibrary.setName(library.getName());
                        refactoringLibrary.setShortDescription(library.getShortDescription());
                        refactoringLibrary.setRefactoringModel(refactoringModel);

                        refactoringLibrary.setAuthor(null);

                        refactoringLibraryController.setEntity(refactoringLibrary);
                        refactoringLibraryController.insert();

                        EList<refactoringModel.refactoringModel.Catalog> catalogs = library.getCatalogs();

                        Catalog catalog = null;
                        Author authorCatalog = null;

                        //Carregando Catálogos
                        for (refactoringModel.refactoringModel.Catalog catalog1 : catalogs) {

                            catalog = new Catalog();
                            catalog.setName(catalog1.getName());
                            catalog.setRefactoringLibrary(refactoringLibrary);

                            authorCatalog = new Author();
                            authorCatalog.setName(catalog1.getAuthor().getName());
                            authorCatalog.setLastName(catalog1.getAuthor().getLastName());
                            authorController.setEntity(authorCatalog);
                            authorController.insert();

                            catalog.setAuthor(authorCatalog);
                            catalogController.setEntity(catalog);
                            catalogController.insert();

                            EList<refactoringModel.refactoringModel.Refactoring> refactorings = catalog1.getRefactorings();

                            Refactoring refactoring = null;
                            Author authorR = null;

//                            PostCondition postCondition = null;
                            Operator operation = null;

                            //Carregando refatorações
                            for (refactoringModel.refactoringModel.Refactoring refactoring1 : refactorings) {

                                refactoring = new Refactoring();
                                refactoring.setName(refactoring1.getName());
                                refactoring.setMotivation(refactoring1.getMotivation());
                                refactoring.setSummary(refactoring1.getSummary());

                                authorR = new Author();
                                authorR.setName(refactoring1.getAuthor().getName());
                                authorR.setLastName(refactoring1.getAuthor().getLastName());
                                authorController.setEntity(authorR);
                                authorController.insert();
                                refactoring.setAuthor(authorR);

                                if (refactoring1.getPreCondition() != null) {
                                    System.out.println("entrou");
                                    PreCondition preCondition = new PreCondition();
                                    preCondition.setBody(refactoring1.getPreCondition().getBody());
                                    preCondition.setContext(refactoring1.getPreCondition().getContext());
                                    preCondition.setLanguage(Language.valueOf(refactoring1.getPreCondition().getLanguage().getName()));
                                    refactoring.setPreCondition(preCondition);
                                } else {
                                    refactoring.setPreCondition(null);
                                }

                                if (refactoring1.getPostCondition() != null) {
                                    System.out.println("entrou2");
                                    PostCondition postCondition = new PostCondition();
                                    postCondition.setBody(refactoring1.getPostCondition().getBody());
                                    postCondition.setContext(refactoring1.getPostCondition().getContext());
                                    postCondition.setLanguage(Language.valueOf(refactoring1.getPostCondition().getLanguage().getName()));
                                    refactoring.setPostCondition(postCondition);
                                } else {
                                    refactoring.setPostCondition(null);
                                }

                                operation = new Operator();
                                operation.setBody(refactoring1.getOperation().getBody());
                                operation.setLanguage(Language.valueOf(refactoring1.getOperation().getLanguage().getName()));
                                refactoring.setOperator(operation);

                                refactoring.setCatalog(catalog);
                                RefactoringRepository refactoringRepository = new RefactoringRepository();
                                refactoringRepository.insert(refactoring);

                                //Carregando classificações
                                EList<refactoringModel.refactoringModel.Classification> classifications = refactoring1.getClassification();

                                Classification classification = null;
                                Level level = null;
                                Usage usage = null;

                                for (refactoringModel.refactoringModel.Classification classification1 : classifications) {

                                    classification = new Classification();

                                    level = new Level();
                                    level.setKind(LevelKind.valueOf(classification1.getLevel().getKind().getName()));
                                    classification.setLevel(level);

                                    usage = new Usage();
                                    usage.setKind(UsageKind.valueOf(classification1.getUsage().getKind().getName()));
                                    classification.setUsage(usage);

                                    classification.setRefactoring(refactoring);

                                    //Carregando KDMPackages
                                    EList<refactoringModel.refactoringModel.KDMPackage> kDMPackages = classification1.getKdmPackage();

                                    KDMPackage kDMPackage = null;

                                    for (refactoringModel.refactoringModel.KDMPackage kDMPackage1 : kDMPackages) {

                                        if (kDMPackage1 != null) {
                                            System.out.println("entrou3");
                                            kDMPackage = new KDMPackage();
                                            kDMPackage.setKind(KDMPackageKind.valueOf(kDMPackage1.getKind().getName()));
                                            classification.getKdmPackage().add(kDMPackage);
                                        }

                                    }

                                    classificationController.setEntity(classification);
                                    classificationController.insert();

                                }

                            }

                        }

                    }

                }

            }

            String nome = "/uploads/" + fileController.getFile().getFileName();
            File f = new File(nome);
            f.delete();

        } catch (RuntimeException exception) {

            System.out.println("Problem loading " + uri);

            exception.printStackTrace();

        }

    }

    public void downloadRefactoring() {

        refactoringModel.refactoringModel.RefactoringModel modelToAdd = RefactoringModelFactory.eINSTANCE.createRefactoringModel();

        refactoringModel.refactoringModel.Refactoring refactoringToSave = RefactoringModelFactory.eINSTANCE.createRefactoring();
        refactoringModel.refactoringModel.Author authorRef = RefactoringModelFactory.eINSTANCE.createAuthor();
        refactoringModel.refactoringModel.PreCondition preCondition = RefactoringModelFactory.eINSTANCE.createPreCondition();
        refactoringModel.refactoringModel.PostCondition postCondition = RefactoringModelFactory.eINSTANCE.createPostCondition();
        refactoringModel.refactoringModel.Operation operation = RefactoringModelFactory.eINSTANCE.createOperation();

        refactoringToSave.setName(this.entity.getName());
        refactoringToSave.setMotivation(this.entity.getMotivation());
        refactoringToSave.setSummary(this.entity.getSummary());
        System.out.println("refname: " + refactoringToSave.getName());

        authorRef.setName(this.entity.getAuthor().getName());
        authorRef.setLastName(this.entity.getAuthor().getLastName());
        refactoringToSave.setAuthor(authorRef);

        if (this.entity.getPreCondition() != null) {
            preCondition.setBody(this.entity.getPreCondition().getBody());
            preCondition.setContext(this.entity.getPreCondition().getContext());
            preCondition.setLanguage(refactoringModel.refactoringModel.Language.valueOf(this.entity.getPreCondition().getLanguage().toString()));
            refactoringToSave.setPreCondition(preCondition);
        }

        if (this.entity.getPostCondition() != null) {
            postCondition.setBody(this.entity.getPostCondition().getBody());
            postCondition.setContext(this.entity.getPostCondition().getContext());
            postCondition.setLanguage(refactoringModel.refactoringModel.Language.valueOf(this.entity.getPostCondition().getLanguage().toString()));
            refactoringToSave.setPostCondition(postCondition);
        }

        operation.setBody(this.entity.getOperator().getBody());
        operation.setLanguage(refactoringModel.refactoringModel.Language.valueOf(this.entity.getOperator().getLanguage().toString()));
        refactoringToSave.setOperation(operation);

        EList<refactoringModel.refactoringModel.Classification> classificationOfRef = refactoringToSave.getClassification();
        refactoringModel.refactoringModel.Classification classificationToAdd = null;
        refactoringModel.refactoringModel.Usage usageToAdd = null;
        refactoringModel.refactoringModel.Level levelToAdd = null;

        List<Classification> classificationsAux = this.classificationController.getByRefactoring(this.entity.getID());

        for (Classification classification1 : classificationsAux) {

            classificationToAdd = RefactoringModelFactory.eINSTANCE.createClassification();

            levelToAdd = RefactoringModelFactory.eINSTANCE.createLevel();
            levelToAdd.setKind(refactoringModel.refactoringModel.LevelKind.valueOf(classification1.getLevel().getKind().toString()));
            classificationToAdd.setLevel(levelToAdd);

            usageToAdd = RefactoringModelFactory.eINSTANCE.createUsage();
            usageToAdd.setKind(refactoringModel.refactoringModel.UsageKind.valueOf(classification1.getUsage().getKind().toString()));
            classificationToAdd.setUsage(usageToAdd);

            //Carregando KDMPackages
            EList<refactoringModel.refactoringModel.KDMPackage> kDMPackages = classificationToAdd.getKdmPackage();
            
            
//            KDMPackageRepository kDMPackageRepository = new KDMPackageRepository();
            List<KDMPackage> kDMPackagesAux = classification1.getKdmPackage();
            
            System.out.println (kDMPackagesAux.size());

            refactoringModel.refactoringModel.KDMPackage kDMPackageToAdd = null;

            for (KDMPackage kDMPackage1 : kDMPackagesAux) {
               
                kDMPackageToAdd = RefactoringModelFactory.eINSTANCE.createKDMPackage();
                kDMPackageToAdd.setKind(refactoringModel.refactoringModel.KDMPackageKind.valueOf(kDMPackage1.getKind().toString()));
                kDMPackages.add(kDMPackageToAdd);

            }

            classificationOfRef.add(classificationToAdd);

        }

        refactoringModel.refactoringModel.Catalog catalogRef = RefactoringModelFactory.eINSTANCE.createCatalog();
        //refactoringModel.refactoringModel.Author authorCat = RefactoringModelFactory.eINSTANCE.createAuthor();

        catalogRef.setName(this.entity.getCatalog().getName());

        //authorCat.setName(this.entity.getCatalog().getAuthor().getName());
        //authorCat.setLastName(this.entity.getCatalog().getAuthor().getLastName());
        catalogRef.setAuthor(authorRef);

        catalogRef.getRefactorings().add(refactoringToSave);

        refactoringModel.refactoringModel.RefactoringLibrary libraryCatalog = RefactoringModelFactory.eINSTANCE.createRefactoringLibrary();
        libraryCatalog.setName(this.entity.getCatalog().getRefactoringLibrary().getName());
        libraryCatalog.setDescription(this.entity.getCatalog().getRefactoringLibrary().getDescription());
        libraryCatalog.setShortDescription(this.entity.getCatalog().getRefactoringLibrary().getShortDescription());
        libraryCatalog.getCatalogs().add(catalogRef);

        refactoringModel.refactoringModel.Author authorModel = RefactoringModelFactory.eINSTANCE.createAuthor();

        Author authorModelAux = this.entity.getCatalog().getRefactoringLibrary().getRefactoringModel().getAuthor();

        if (authorModelAux != null) {
            System.out.println("authorModel nao nulo");
            authorModel.setName(this.entity.getCatalog().getRefactoringLibrary().getRefactoringModel().getAuthor().getName());
            authorModel.setLastName(this.entity.getCatalog().getRefactoringLibrary().getRefactoringModel().getAuthor().getLastName());
            modelToAdd.setAuthor(authorModel);
        }

        modelToAdd.setAuthor(authorRef);
        modelToAdd.getLibraries().add(libraryCatalog);
        System.out.println(modelToAdd);

        ResourceSet resourceSet = new ResourceSetImpl();

// Register the appropriate resource factory to handle all file extensions.
//
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
                new XMIResourceFactoryImpl());

// Register the package to ensure it is available during loading.
//
        resourceSet.getPackageRegistry().put(RefactoringModelPackage.eNS_URI,
                RefactoringModelPackage.eINSTANCE);

// If there are no arguments, emit an appropriate usage message.
//
        System.out.println("Enter a list of file paths or URIs that have content like this:");
        try {
            Resource resource = resourceSet.createResource(URI.createURI("file:/uploads/" + ApplicationUtilies.generateURL(this.entity.getName()) + ".refactoringmodel"));
            //Model root = RefactoringFactory.eINSTANCE.createModel();
            System.out.println(resource.getURI());
            resource.getContents().add(modelToAdd);
            //criar arquivo
            resource.save(null);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        File file = new File("/uploads/" + ApplicationUtilies.generateURL(this.entity.getName()) + ".refactoringmodel");
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        response.setHeader("Content-Disposition", "attachment;filename=" + ApplicationUtilies.generateURL(this.entity.getName()) + ".refactoringmodel");
        response.setContentLength((int) file.length());
        ServletOutputStream out = null;
        try {
            FileInputStream input = new FileInputStream(file);

            //System.gc();
            //deletar arquivo
            byte[] buffer = new byte[1024];
            out = response.getOutputStream();
            int i = 0;
            while ((i = input.read(buffer)) != -1) {
                out.write(buffer);
                out.flush();
            }

            //fecha as coisas inútil
            input.close();
            file.delete();

            FacesContext.getCurrentInstance().getResponseComplete();
        } catch (IOException err) {
            err.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException err) {
                err.printStackTrace();
            }
        }

    }

}
