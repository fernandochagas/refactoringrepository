/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.commons.io.FileUtils;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Fernando
 */
@Named
@SessionScoped
public class FileController implements Serializable {

    private UploadedFile file;
    
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void upload() {
        if (file != null) {
            System.err.println("name " + file.getFileName());
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);

            //get uploaded file from the event         

            //create an InputStream from the uploaded file
            InputStream inputStr = null;
            try {
                inputStr = file.getInputstream();
            } catch (IOException e) {
                //log error
            }

            //create destination File
            String destPath = "/uploads/" + file.getFileName();
            File destFile = new File(destPath);

            //use org.apache.commons.io.FileUtils to copy the File
            try {
                FileUtils.copyInputStreamToFile(inputStr, destFile);
            } catch (IOException e) {
                //log error
            }
        } else {
            System.err.println("erro");
        }
    }

}
