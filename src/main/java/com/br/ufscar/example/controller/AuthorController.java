/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.controller;

import com.br.ufscar.example.generic.GenericController;
import com.br.ufscar.example.model.Author;
import com.br.ufscar.example.repository.AuthorRepository;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Fernando
 */
@Named
@SessionScoped
public class AuthorController extends GenericController<Author, AuthorRepository> {

    public AuthorController() {
        this.entity = new Author();
    }

    @Override
    public void insert() {
        this.repository = new AuthorRepository();
        this.repository.insert(this.entity);
        this.entity = new Author();

        //message
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "New Author added"));
    }

    @Override
    public void remove() {
        this.repository = new AuthorRepository();
        this.repository.remove(this.entity);
        this.entity = new Author();
    }

    @Override
    public void update() {
        this.repository = new AuthorRepository();
        this.repository.update(this.entity);
        this.entity = new Author();
    }

    @Override
    public List<Author> getListOfEntities() {
        this.repository = new AuthorRepository();
        this.listOfEntities = this.repository.getListOfElements();
        return this.listOfEntities;

    }
    
    public List<Author> completeAuthor(String query) {
        this.repository = new AuthorRepository();
        List<Author> filteredAuthors = new ArrayList<>();
        this.listOfEntities = this.repository.getListOfElements();
        for (int i = 0; i < this.listOfEntities.size(); i++) {
            String nameAuthor = this.listOfEntities.get(i).getName() + " " + this.listOfEntities.get(i).getLastName();
            if (nameAuthor.toLowerCase().contains(query.toLowerCase())) {
                filteredAuthors.add(this.listOfEntities.get(i));
            }
        }
        return filteredAuthors;
    }

}
