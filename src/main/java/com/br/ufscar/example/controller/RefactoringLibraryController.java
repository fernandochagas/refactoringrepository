/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.controller;

import com.br.ufscar.example.generic.GenericController;
import com.br.ufscar.example.model.Author;
import com.br.ufscar.example.model.RefactoringLibrary;
import com.br.ufscar.example.model.RefactoringModel;
import com.br.ufscar.example.repository.RefactoringLibraryRepository;
import com.br.ufscar.example.repository.RefactoringModelRepository;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Fernando
 */
@Named
@SessionScoped
public class RefactoringLibraryController extends GenericController<RefactoringLibrary, RefactoringLibraryRepository> {

    public RefactoringLibraryController() {
        this.entity = new RefactoringLibrary();
    }

    @Override
    public void insert() {
        this.repository = new RefactoringLibraryRepository();
        
        RefactoringModel refactoringModel = new RefactoringModel();     
        RefactoringModelRepository refactoringModelRepository = new RefactoringModelRepository();
        refactoringModelRepository.insert(refactoringModel);
        
        this.entity.setRefactoringModel(refactoringModel);
        this.repository.insert(this.entity);
        this.entity = new RefactoringLibrary();

        //message
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "New RefactoringLibrary added"));
    }

    @Override
    public void remove() {
        this.repository = new RefactoringLibraryRepository();
        this.repository.remove(this.entity);
        this.entity = new RefactoringLibrary();
    }

    @Override
    public void update() {
        this.repository = new RefactoringLibraryRepository();
        this.repository.update(this.entity);
        this.entity = new RefactoringLibrary();
    }

    public List<String> authors() {
        List<String> authors = new ArrayList<>();
        return authors;
    }

    @Override
    public List<RefactoringLibrary> getListOfEntities() {
        this.repository = new RefactoringLibraryRepository();
        this.listOfEntities = this.repository.getListOfElements();
        return this.listOfEntities;

    }
    
    public List<RefactoringLibrary> completeRefatoringLibrary (String query) {
        this.repository = new RefactoringLibraryRepository();
        List<RefactoringLibrary> filteredLibraries = new ArrayList<>();
        this.listOfEntities = this.repository.getListOfElements();
        for (int i = 0; i < this.listOfEntities.size(); i++) {
            String refactoringLibraryName = this.listOfEntities.get(i).getName();
            if (refactoringLibraryName.toLowerCase().contains(query.toLowerCase())) {
                filteredLibraries.add(this.listOfEntities.get(i));
            }
        }
        return filteredLibraries;
    }

}
