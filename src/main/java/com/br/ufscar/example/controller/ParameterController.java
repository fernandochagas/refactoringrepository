/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.controller;

import com.br.ufscar.example.generic.GenericController;
import com.br.ufscar.example.model.Parameter;
import com.br.ufscar.example.model.Refactoring;
import com.br.ufscar.example.repository.ParameterRepository;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @parameter Fernando
 */
@Named
@SessionScoped
public class ParameterController extends GenericController<Parameter, ParameterRepository> {

    public ParameterController() {
        this.entity = new Parameter();
    }

    @Override
    public void insert() {
        this.repository = new ParameterRepository();
        this.repository.insert(this.entity);
        this.entity = new Parameter();

        //message
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "New Parameter added"));
    }

    @Override
    public void remove() {
        this.repository = new ParameterRepository();
        this.repository.remove(this.entity);
        this.entity = new Parameter();
    }

    @Override
    public void update() {
        this.repository = new ParameterRepository();
        this.repository.update(this.entity);
        this.entity = new Parameter();
    }

    @Override
    public List<Parameter> getListOfEntities() {
        this.repository = new ParameterRepository();
        this.listOfEntities = this.repository.getListOfElements();
        return this.listOfEntities;
    }
    
    public List<Parameter> getByRefactoring (Long refactoringId) {
        this.repository = new ParameterRepository();
        return this.repository.getByRefactoring(refactoringId);
    }
    
}
