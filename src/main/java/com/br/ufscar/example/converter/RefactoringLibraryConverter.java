/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.ufscar.example.converter;

import com.br.ufscar.example.model.RefactoringLibrary;
import com.br.ufscar.example.repository.RefactoringLibraryRepository;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("rLibraryConverter")
public class RefactoringLibraryConverter implements Converter {

    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
                RefactoringLibraryRepository repository = new RefactoringLibraryRepository();
                return repository.getById(Long.parseLong(value));
            } catch (NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "RefactoringLibrary Error", "Refactoring Library validator has encountered an error."));
            }
        } else {
            return null;
        }
    }

    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            return String.valueOf(((RefactoringLibrary) object).getID());
        } else {
            return null;
        }
    }
}
